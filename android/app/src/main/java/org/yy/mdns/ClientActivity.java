package org.yy.mdns;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public class ClientActivity extends AppCompatActivity {
    private static final String TAG = "ClientActivity";
    private RecyclerView recyclerView;
    private NsdManager mNsdManager = null;
    private NsdManager.DiscoveryListener mNSDDiscoveryListener = null;    //    搜寻监听器
    private NsdManager.ResolveListener mNSDResolveListener = null;//    解析监听器

    private NSDDeviceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NSDDeviceAdapter();
        recyclerView.setAdapter(adapter);
        mNSDDiscoveryListener = new NsdManager.DiscoveryListener() {
            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.i(TAG,"onStartDiscoveryFailed ");

            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.i(TAG,"onStopDiscoveryFailed ");

            }

            @Override
            public void onDiscoveryStarted(String serviceType) {
                Log.i(TAG,"onDiscoveryStarted ");

            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG,"onDiscoveryStopped ");

            }

            @Override
            public void onServiceFound(NsdServiceInfo serviceInfo) {
                Log.i(TAG,"onServiceFound "+serviceInfo.toString());
//                if(serviceInfo.getServiceName().equals("testclient")){
//                    return;
//                }
                mNsdManager.resolveService(serviceInfo, mNSDResolveListener);

            }

            @Override
            public void onServiceLost(final NsdServiceInfo serviceInfo) {
                Log.i(TAG,"onServiceLost ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.removeDevice(serviceInfo);
                    }
                });
            }
        };

        mNSDResolveListener = new NsdManager.ResolveListener() {
            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                Log.i(TAG,"onResolveFailed ");

            }

            @Override
            public void onServiceResolved(final NsdServiceInfo serviceInfo) {
                Log.i(TAG,"onServiceResolved ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.addDevice(serviceInfo);

                    }
                });
            }
        };
        mNsdManager = (NsdManager) getSystemService(NSD_SERVICE);
        mNsdManager.discoverServices("_test._udp.", NsdManager.PROTOCOL_DNS_SD, mNSDDiscoveryListener);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mNsdManager.stopServiceDiscovery(mNSDDiscoveryListener);
    }

}
